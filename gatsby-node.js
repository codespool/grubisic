const path = require('path')

exports.createPages = ({ boundActionCreators, graphql }) => {
  const { createPage } = boundActionCreators

  const newsTemplate = path.resolve(`src/templates/newsTemplate.js`)
  const serviceTemplate = path.resolve(`src/templates/serviceTemplate.js`)
  return graphql(
    `
    {
      news: allContentfulNews (filter: {node_locale: {regex: "/en/"}}) {
       edges {
           node {
             id
             title
             slug
             createdAt
             image {
               id
               resolutions(width: 600){
                 src
               }
             }
             content {
               id
               childMarkdownRemark{
                 html
               }
             }
           }
         }
     },
     services: allContentfulServices (filter: {node_locale: {regex: "/en/"}}) {
      edges {
          node {
            id
            title
            slug
            createdAt
            image {
              id
              resolutions(width: 600){
                src
              }
            }
            content {
              id
              childMarkdownRemark{
                html
              }
            }
          }
        }
    }
     }
  `
  ).then(result => {
    if (result.errors) {
      return Promise.reject(result.errors)
    }

    result.data.news.edges.forEach(({ node }) => {
      createPage({
        path: `news/${node.slug}`,
        component: newsTemplate,
        context: {
          slug: node.slug
        } // additional data can be passed via context
      })
    })

    result.data.services.edges.forEach(({ node }) => {
      createPage({
        path: `services/${node.slug}`,
        component: serviceTemplate,
        context: {
          slug: node.slug
        } // additional data can be passed via context
      })
    })
  })
}
