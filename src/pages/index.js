import React, { Component } from 'react'
import g from 'glamorous'
import { Layout } from '../components/Layout'
import { mediaQueries } from '../utils/shared'
import { NewsThumb } from '../components/NewsThumb'
import { CentralTitle } from '../components/CentralTitle'

export class IndexPage extends Component {
  constructor() {
    super()
    this.timer = null
  }
  state = {
    factSheet: '',
    title: '',
    subtitle: ''
  }
  componentDidMount() {
    this.props.data.homePageContent.edges.forEach(pageContent => {
      new window.Image().src = pageContent.node.background.sizes.src
      // caches images, avoiding white flash between background replacements
    })
    this.backgroundSequence()
  }
  componentWillUnmount() {
    this.clearAllTimeouts()
  }
  clearAllTimeouts = () => {
    while (this.timer) {
      window.clearTimeout(this.timer)
      this.timer--
    }
  }
  backgroundSequence = () => {
    const { data: { homePageContent: { edges: contentList } } } = this.props
    const secs = 5
    this.clearAllTimeouts()
    var k = 0
    for (let i = 0; i < contentList.length; i++) {
      this.timer = setTimeout(() => {
        this.background.style.background = `url(${contentList[k].node.background.sizes.src}) no-repeat center center`
        this.background.style.backgroundSize = 'cover'
        this.factSheet.style.background = `url(${contentList[k].node.factSheet.sizes.src}) no-repeat center center`
        this.factSheet.style.backgroundSize = 'contain'
        this.setState({
          title: contentList[k].node.title,
          subtitle: contentList[k].node.text.childMarkdownRemark.html
        })
        if (k + 1 === contentList.length) {
          this.timer = setTimeout(() => {
            this.backgroundSequence()
          }, secs * 1000)
        } else {
          k++
        }
      }, secs * 1000 * i)
    }
  }
  render () {
    const {
      data: { homePageContent: { edges: contentList }, latestNews },
      serviceList,
      team
    } = this.props
    const News = latestNews.edges.map(newsItem => (
      <NewsThumb key={newsItem.node.id} newsItem={newsItem} />
    ))
    return (
      <Layout serviceList={serviceList} team={team} >
        <Wrapper>
          <Background
            innerRef={elem => {
              this.background = elem
            }}
            css={{ background: `url(${contentList[0].node.background.sizes.src}) no-repeat center center` }}
          />
          <BackgroundOverlay />
          <TitleWrapper>
            <FactSheet innerRef={elem => {
              this.factSheet = elem
            }}
            />
            <CentralTitle title={this.state.title} subtitle={this.state.subtitle} />
          </TitleWrapper>
          <NewsWrapper>
            {News}
          </NewsWrapper>
        </Wrapper>
      </Layout>
    )
  }
}

const TitleWrapper = g.div({
  width: '80vw',
  display: 'flex',
  alignSelf: 'center',
  justifySelf: 'center'
})
const FactSheet = g.div({
  minWidth: '120px',
  minHeight: '120px',
  transition: '0.5s ease-out 1s',
  backgroundSize: 'contain',
  alignSelf: 'flex-start',
  [mediaQueries.mid]: {
    minWidth: '170px',
    minHeight: '170px'
  },
  [mediaQueries.large]: {
    minWidth: '200px',
    minHeight: '200px'
  }
})
const Background = g.div({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100vw',
  minWidth: '360px',
  height: '100vh',
  paddingTop: '5em',
  backgroundSize: 'cover',
  zIndex: -10,
  transition: '0.5s ease-out 1s'
})
const BackgroundOverlay = g.div({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100vw',
  minWidth: '360px',
  height: '100vh',
  paddingTop: '5em',
  backgroundColor: 'rgba(0, 0, 0, 0.2)',
  zIndex: -5
})
const Wrapper = g.div({
  height: '220vh',
  display: 'grid',
  paddingTop: '5rem',
  gridTemplateRows: '100vh 1fr',
  [mediaQueries.mid]: {
    height: '100vh',
    gridTemplateRows: '50% 1fr'
  },
  [mediaQueries.large]: {
    height: '100vh',
    gridTemplateRows: '70% 1fr'
  }
})
const NewsWrapper = g.div({
  display: 'grid',
  gridTemplateRows: '1fr 1fr 1fr 1fr',
  gridTemplateColumns: '1fr',
  justifyItems: 'stretch',
  padding: 0,
  [mediaQueries.mid]: {
    gridTemplateRows: '1fr 1fr',
    gridTemplateColumns: '1fr 1fr',
    justifyItems: 'center',
    padding: '0 15%'
  },
  [mediaQueries.large]: {
    gridTemplateRows: '1fr',
    gridTemplateColumns: '1fr 1fr 1fr 1fr',
    padding: '1rem'
  }
})

export default IndexPage

export const pageQuery = graphql`
  query IndexQuery {
    homePageContent: allContentfulHomePageSlides (sort: {fields:[displayOrder], order: ASC}){
    edges {
      node{
        title
        factSheet {
          sizes(maxWidth: 600) {
            src
          }
        }
        background {
          sizes(maxWidth: 2300) {
            src
          }
        }
        text{childMarkdownRemark{html}}
        }
      }
    },
  latestNews: allContentfulNews(limit: 4, sort: {fields: [ createdAt ],order: DESC}){
    edges {
      node {
        id,
        title,
        slug,
        image{
          sizes(maxWidth: 800){
            src
          }
        }
      }
    }
  }
  }
`
