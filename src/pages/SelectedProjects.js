import React, { Component } from 'react'
import g from 'glamorous'
import { mediaQueries, uuid } from '../utils/shared'
import { Layout } from '../components/Layout'
import { HorizontalMenu } from '../components/HorizontalMenu'

import Media from 'react-media'
export default class SelectedProjects extends Component {
  state = {
    isModalOpen: false,
    modalContentSource: null
  }
  handleToombstoneClick = (event, toombstoneSrc) => {
    event.preventDefault()
    this.setState(currentState => ({
      isModalOpen: true,
      modalContentSource: toombstoneSrc
    }))
  }
  toggleModal = modalNextState => {
    this.setState({ isModalOpen: modalNextState })
  }
  render () {
    const { data, serviceList, team } = this.props
    const { background, toombstones } = data
    return (
      <Layout title='Selected projects' serviceList={serviceList} team={team}>
        <Media
          query='(min-width: 800px)'
          render={() => (
            <ModalOverlay
              isActive={this.state.isModalOpen}
              onClick={() => this.toggleModal(false)}
            >
              <ModalImageWrapper>
                <ModalImage src={this.state.modalContentSource} />
              </ModalImageWrapper>
            </ModalOverlay>
          )}
        />
        <Wrapper>
          <MenuWrapper>
            <HorizontalMenu serviceList={serviceList} />
          </MenuWrapper>
          <NewsHero
            css={{
              background: `url(${background.image.sizes.src}) no-repeat center center`
            }}
          >
            <NewsText>
              <TitleWrapper>
                <h1>We have closed over XXX projects</h1>
                <h2>On 5 continents</h2>
              </TitleWrapper>
              <NewsBody>
                See the details below
              </NewsBody>
            </NewsText>
          </NewsHero>
          <Media query='(min-width: 800px)'>
            {doesMatch => (
              <ToombstoneList>
                {toombstones.edges.map(toombstone => (
                  <Toombstone key={uuid()}>
                    <ToombstoneImage
                      onClick={
                        doesMatch
                          ? event =>
                              this.handleToombstoneClick(
                                event,
                                toombstone.node.toombstone.sizes.src
                              )
                          : null
                      }
                      src={toombstone.node.toombstone.sizes.src}
                    />
                  </Toombstone>
                ))}
              </ToombstoneList>
            )}
          </Media>
        </Wrapper>
      </Layout>
    )
  }
}

const MenuWrapper = g.div({
  display: 'none',
  [mediaQueries.large]: {
    display: 'block',
    borderBottom: '1px solid white'
  }
})
const ModalImageWrapper = g.div({
  width: '60vw',
  maxWidth: '500px',
  maxHeight: '80vh'
})
const ModalImage = g.img({
  width: '100%',
  height: 'auto'
})
const ModalOverlay = g.div(
  {
    position: 'fixed',
    top: '0',
    left: '0',
    width: '100vw',
    height: '100vh',
    background: 'rgba(0, 0, 0, 0.5)',
    zIndex: '100',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  ({ isActive }) => ({
    display: `${isActive ? 'flex' : 'none'}`
  })
)
const Toombstone = g.div({
  width: '80vw',
  height: 'auto',
  padding: '0 1rem',
  [mediaQueries.mid]: {
    width: '300px',
    minWidth: '300px'
  }
})
const ToombstoneImage = g.img({
  width: '100%',
  height: 'auto',
  border: '1px solid rgb(241,16,30)',
  cursor: 'pointer',
  '&:hover': {
    boxShadow: 'rgb(241,16,30) 0 0 1rem'
  }
})
const Wrapper = g.div({
  paddingTop: '5em',
  minHeight: '100vh'
})
const NewsText = g.div({
  width: '80vw',
  background: 'rgba(0, 0, 0, 0.4)',
  padding: '2rem',
  fontWeight: 'bold',
  [mediaQueries.mid]: {
    width: '60vw'
  }
})
const TitleWrapper = g.div({
  fontFamily: 'Lato',
  color: 'white',
  textAlign: 'left',
  borderLeft: '5px solid red',
  paddingLeft: '1rem',
  '& h1': {
    fontSize: '1.5rem'
  },
  '& h2': {
    fontSize: '1.2rem'
  },
  [mediaQueries.mid]: {
    '& h1': {
      fontSize: '2rem'
    },
    '& h2': {
      fontSize: '1.5rem'
    }
  }
})
const NewsBody = g.div({
  color: 'white',
  fontFamily: 'Source Serif Pro',
  textAlign: 'left'
})
const NewsHero = g.div({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  width: '100vw',
  minWidth: '360px',
  height: '100vh',
  backgroundSize: 'cover',
  zIndex: -10,
  [mediaQueries.mid]: {
    height: '60vh'
  }
})
const ToombstoneList = g.div({
  display: 'flex',
  flexFlow: 'row wrap',
  padding: '2rem',
  justifyContent: 'space-evenly'
})

export const pageQuery = graphql`
  query selectedProjectsQuery {
    background: contentfulBackgrounds(page:{ eq: "projects"}) {
      image {
      sizes(maxWidth:1920){
        src
      }
    }
  },
  toombstones: allContentfulProjects{
    edges{
      node {
        id
        toombstone{
          sizes(maxWidth:800){
            src
          }
        }
      }
    }
  }
  }
`
