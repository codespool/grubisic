import React from 'react'
import { Layout } from '../components/Layout'
import g from 'glamorous'
import { HorizontalMenu } from '../components/HorizontalMenu'
import { mediaQueries } from '../utils/shared'
import { TeamMembersGrid } from '../components/TeamMembersGrid'

export default function AboutUs ({
  data, // this prop will be injected by the GraphQL query below.
  serviceList,
  team
}) {
  const { image, title, content } = data.pageData // data.markdownRemark holds our post data
  return (
    <Layout title={title} serviceList={serviceList} team={team} >
      <Wrapper paddingTop='5rem'>
        <MenuWrapper>
          <HorizontalMenu serviceList={serviceList} />
        </MenuWrapper>
        <ContentWrapper>
          <ImageWrapper src={image.sizes.src} />
          <TextWrapper>
            <div
              dangerouslySetInnerHTML={{
                __html: content.childMarkdownRemark.html
              }}
            />
          </TextWrapper>
        </ContentWrapper>
        <TeamMembersGrid team={team} />
      </Wrapper>
    </Layout>
  )
}

const Wrapper = g.div({
  paddingTop: '5rem',
  [mediaQueries.large]: {
    paddingTop: '8rem'
  }
})
const MenuWrapper = g.div({
  display: 'none',
  [mediaQueries.large]: {
    display: 'block',
    borderBottom: '1px solid white'
  }
})
const ContentWrapper = g.div({
  display: 'flex',
  flexDirection: 'column',
  [mediaQueries.mid]: {
    flexDirection: 'row'
  }
})
const ImageWrapper = g.div(
  {
    height: '60vh',
    [mediaQueries.mid]: {
      width: '50vw'
    }
  },
  ({ src }) => ({
    background: `url(${src})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover'
  })
)
const TextWrapper = g.div({
  width: '100vw',
  padding: '2rem',
  textAlign: 'left',
  fontFamily: 'Lato',
  [mediaQueries.mid]: {
    width: '50vw'
  },
  [mediaQueries.large]: {
    padding: '3rem'
  }
})
export const pageQuery = graphql`
query aboutPageQuery{
  pageData: contentfulPages(name: {eq: "about_us"}) {
      title
      image {
          sizes(maxWidth: 900, maxHeight: 900) {
              src
          }
      }
      content {
          childMarkdownRemark {
              html
          }
      }
  }
}
`
