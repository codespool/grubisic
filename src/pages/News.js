import React from 'react'
import g from 'glamorous'
import { mediaQueries, uuid } from '../utils/shared'
import { Layout } from '../components/Layout'
import { NewsCard } from '../components/NewsCard'

export default function News ({ data, serviceList, team }) {
  const {
    background = { image: { sizes: {} } },
    allNews: { edges: allNews }
  } = data

  return (
    <Layout title='Selected projects' serviceList={serviceList} team={team}>
      <Wrapper>
        <NewsHero
          css={{
            background: `url(${background.image.sizes.src}) no-repeat center center`
          }}
        >
          <NewsText>
            <TitleWrapper>
              <h1>Here you can find the latest news</h1>
              <h2>About the company and projects</h2>
            </TitleWrapper>
            <NewsBody>
              Click the news to see more details
            </NewsBody>
          </NewsText>
        </NewsHero>
        <NewsList>
          {allNews.map(newsItem => (
            <NewsCard key={uuid()} newsItem={newsItem} />
          ))}
        </NewsList>
      </Wrapper>
    </Layout>
  )
}

const Wrapper = g.div({
  paddingTop: '5em',
  minHeight: '100vh'
})
const NewsText = g.div({
  width: '80vw',
  background: 'rgba(0, 0, 0, 0.4)',
  padding: '2rem',
  fontWeight: 'bold',
  [mediaQueries.mid]: {
    width: '60vw'
  }
})
const TitleWrapper = g.div({
  fontFamily: 'Lato',
  color: 'white',
  textAlign: 'left',
  borderLeft: '5px solid red',
  paddingLeft: '1rem',
  '& h1': {
    fontSize: '1.5rem'
  },
  '& h2': {
    fontSize: '1.2rem'
  },
  [mediaQueries.mid]: {
    '& h1': {
      fontSize: '2rem'
    },
    '& h2': {
      fontSize: '1.5rem'
    }
  }
})
const NewsBody = g.div({
  color: 'white',
  fontFamily: 'Source Serif Pro',
  textAlign: 'left'
})
const NewsHero = g.div({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  width: '100vw',
  minWidth: '360px',
  height: '100vh',
  backgroundSize: 'contain',
  zIndex: -10,
  [mediaQueries.mid]: {
    height: '60vh'
  }
})
const NewsList = g.div({
  display: 'flex',
  flexFlow: 'row wrap',
  padding: '2rem',
  justifyContent: 'space-evenly'
})

export const pageQuery = graphql`
  query newsHomeQuery {
    background: contentfulBackgrounds(page:{ eq: "news"}) {
      image {
      sizes(maxWidth:1920){
        src
      }
    }
  },
  allNews: allContentfulNews(sort: {fields: [ createdAt ],order: DESC}){
    edges {
      node {
        id,
        title,
        slug,
        displayDate,
        image{
          sizes(maxWidth: 400){
            src
          }
        },
        content {
          content
          childMarkdownRemark {
              html
              excerpt(pruneLength:150)
          }
        }
      }
    }
  }
  }
`
