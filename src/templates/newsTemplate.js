import React from 'react'
import g from 'glamorous'
import { mediaQueries, uuid } from '../utils/shared'
import { Layout } from '../components/Layout'
import { NewsCard } from '../components/NewsCard'

export default function Template ({ data, serviceList, team }) {
  const { image, title, content, displayDate } = data.selectedNews
  const { edges: allNews } = data.allNews
  return (
    <Layout title='News and announcements' serviceList={serviceList} team={team}>
      <Wrapper>
        <NewsHero css={{ background: `url(${image.sizes.src}) no-repeat center center` }}>
          <div>{'<'}</div>
          <NewsText>
            <TitleWrapper>
              <h1>{title}</h1>
              <h2>{displayDate}</h2>
            </TitleWrapper>
            <NewsBody
              dangerouslySetInnerHTML={{
                __html: content.childMarkdownRemark.html
              }}
            />
          </NewsText>
          <div>{'>'}</div>
        </NewsHero>
        <NewsList>
          {allNews.map(newsItem => <NewsCard key={uuid()} newsItem={newsItem} />)}
        </NewsList>
      </Wrapper>
    </Layout>
  )
}

const Wrapper = g.div({
  paddingTop: '5em',
  minHeight: '100vh'
})
const NewsText = g.div({
  width: '80vw',
  background: 'rgba(0, 0, 0, 0.4)',
  padding: '2rem',
  fontWeight: 'bold',
  [mediaQueries.mid]: {
    width: '60vw'
  }
})
const TitleWrapper = g.div({
  fontFamily: 'Lato',
  color: 'white',
  textAlign: 'left',
  borderLeft: '5px solid red',
  paddingLeft: '1rem',
  '& h1': {
    fontSize: '1.5rem'
  },
  '& h2': {
    fontSize: '1.2rem'
  },
  [mediaQueries.mid]: {
    '& h1': {
      fontSize: '2rem'
    },
    '& h2': {
      fontSize: '1.5rem'
    }
  }
})
const NewsBody = g.div({
  color: 'white',
  fontFamily: 'Source Serif Pro',
  textAlign: 'left'
})
const NewsHero = g.div({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  width: '100vw',
  minWidth: '360px',
  height: '100vh',
  backgroundSize: 'contain',
  zIndex: -10,
  [mediaQueries.mid]: {
    height: '60vh'
  }
})
const NewsList = g.div({
  display: 'flex',
  flexFlow: 'row wrap',
  padding: '2rem',
  justifyContent: 'space-evenly'
})

export const pageQuery = graphql`
query newsQuery($slug: String!){
  selectedNews: contentfulNews(slug: {eq: $slug}) {
      title
      createdAt(formatString: "MMMM DD, YYYY")
      displayDate
      image {
          sizes(maxWidth: 2300) {
              src
          }
      }
      content {
          childMarkdownRemark {
              html
          }
      }
  },
  allNews: allContentfulNews(sort: {fields: [ createdAt ],order: DESC}){
    edges {
      node {
        id,
        title,
        slug,
        displayDate,
        image{
          sizes(maxWidth: 400){
            src
          }
        },
        content {
          content
          childMarkdownRemark {
              html
              excerpt(pruneLength:200)
          }
        }
      }
    }
  }
}
`
