import React from 'react'
import g from 'glamorous'
import { mediaQueries } from '../utils/shared'
import { Layout } from '../components/Layout'
import { HorizontalMenu } from '../components/HorizontalMenu'

export default function Template ({
  data, // this prop will be injected by the GraphQL query below.
  serviceList,
  team
}) {
  const { image, title, content } = data.contentfulServices // data.markdownRemark holds our post data
  return (
    <Layout title='Services' serviceList={serviceList} team={team}>
      <Wrapper paddingTop='5rem'>
        <MenuWrapper>
          <HorizontalMenu serviceList={serviceList} />
        </MenuWrapper>
        <ServicesHero css={{ background: `url(${image.sizes.src}) no-repeat center center` }}>
          <NewsText>
            <TitleWrapper>
              <h1>{title}</h1>
            </TitleWrapper>
          </NewsText>
        </ServicesHero>
        <ServicesText dangerouslySetInnerHTML={{
          __html: content.childMarkdownRemark.html
        }} />
      </Wrapper>
    </Layout>
  )
}

const MenuWrapper = g.div({
  display: 'none',
  [mediaQueries.large]: {
    display: 'block',
    borderBottom: '1px solid white'
  }
})

const Wrapper = g.div({
  paddingTop: '5em',
  minHeight: '100vh'
})
const NewsText = g.div({
  width: '80vw',
  background: 'rgba(0, 0, 0, 0.4)',
  padding: '2rem',
  fontWeight: 'bold',
  [mediaQueries.mid]: {
    width: '60vw'
  }
})
const TitleWrapper = g.div({
  fontFamily: 'Lato',
  color: 'white',
  textAlign: 'left',
  borderLeft: '5px solid red',
  paddingLeft: '1rem',
  '& h1': {
    fontSize: '1.5rem'
  },
  '& h2': {
    fontSize: '1.2rem'
  },
  [mediaQueries.mid]: {
    '& h1': {
      fontSize: '2rem'
    },
    '& h2': {
      fontSize: '1.5rem'
    }
  }
})
const NewsBody = g.div({
  color: 'white',
  fontFamily: 'Source Serif Pro',
  textAlign: 'left'
})
const ServicesHero = g.div({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  width: '100vw',
  minWidth: '360px',
  height: '100vh',
  backgroundSize: 'contain',
  zIndex: -10,
  [mediaQueries.mid]: {
    height: '60vh'
  }
})
const ServicesText = g.div({
  margin: '0 10vw',
  width: '80vw',
  textAlign: 'left'
})

export const pageQuery = graphql`
query serviceQuery($slug: String!){
  contentfulServices(slug: {eq: $slug}) {
      title
      image {
          sizes(maxWidth: 2300) {
              src
          }
      }
      content {
          childMarkdownRemark {
              html
          }
      }
  }
}
`
