import Typography from 'typography'

const typography = new Typography({
  baseFontSize: '16px',
  googleFonts: [
    {
      name: 'Lato',
      styles: ['700', 'Bold', 'Black']
    },
    {
      name: 'Source Serif Pro',
      styles: ['400', '400i', '700', '700i']
    }
  ],
  headerFontFamily: ['Lato', 'sans-serif'],
  bodyFontFamily: ['Source Serif Pro', 'serif']
})

export default typography
