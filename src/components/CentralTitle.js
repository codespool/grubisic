import React from 'react'
import g, { H1, Span } from 'glamorous'
import { mediaQueries } from '../utils/shared'

export const CentralTitle = ({ title, subtitle, children }) => {
  return (
    
      <InfoWrapper>
        <Title>{title}</Title>
        <Subtitle dangerouslySetInnerHTML={{
          __html: subtitle
        }} />
      </InfoWrapper>
    
  )
}

const InfoWrapper = g.div({
  marginLeft: '2rem',
  display: 'flex',
  flexDirection: 'column',
  textAlign: 'left'
})

const Title = g.h1({
  color: 'white',
  borderLeft: '0.5rem solid #f1101e',
  fontSize: '1.2rem',
  paddingLeft: '0.5rem',
  [mediaQueries.mid]: {
    fontSize: '1.5rem',
    paddingLeft: '1rem'
  },
  [mediaQueries.large]: {
    fontSize: '2rem'
  }
})

const Subtitle = g.span({
  color: 'white',
  alignSelf: 'flex-start',
  fontSize: '1rem',
  [mediaQueries.mid]: {
    fontWeight: 'bold',
    fontSize: '1.2rem'
  },
  [mediaQueries.large]: {
    fontSize: '1.7rem'
  }
})
