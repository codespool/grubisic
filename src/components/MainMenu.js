import React, { Component } from 'react'
import Link, { withPrefix } from 'gatsby-link'
import g from 'glamorous'
import closeIcon from '../assets/icons/Close_hq.png'
import { func } from 'prop-types'
import { uuid } from '../utils/shared'
import arrow from '../assets/icons/Arrow-right_2_hq.png'
export class MainMenu extends Component {
  static propTypes = {
    closeMenuHandler: func
  }
  state = {
    isSubMenuOpen: false
  }

  handleSubMenu = e => {
    e.preventDefault()
    this.setState(({ isSubMenuOpen }) => ({ isSubMenuOpen: !isSubMenuOpen }))
  }

  render () {
    const { closeMenuHandler, serviceList } = this.props
    return (
      <div>
        <div>
          <Close src={closeIcon} onClick={closeMenuHandler} />
        </div>
        <OuterUl>
          <li><StyledLink to='/'>Home</StyledLink></li>
          <li>
            <StyledLink to='/SelectedProjects'>Selected Projects</StyledLink>
          </li>
          <li><StyledLink to='/AboutUs'>About Us</StyledLink></li>
          <li>
            <SubMenuSwitchBlock onClick={this.handleSubMenu}>
              <SubMenuSwitchIcon isSubMenuOpen={this.state.isSubMenuOpen} />
              <SubMenuSwitchText>Services</SubMenuSwitchText>
            </SubMenuSwitchBlock>

            <InnerUl isSubMenuOpen={this.state.isSubMenuOpen}>
              {serviceList.map(service => <li key={uuid()}>
                <StyledLink to={`/services/${service.node.slug}`}>{service.node.title}</StyledLink>
              </li>)}
            </InnerUl>
          </li>
          <li><StyledLink to='/News'>News</StyledLink></li>
          <li><StyledLink to='/Clients'>Clients</StyledLink></li>
          <li><BrochureLink href={withPrefix('/grubisicpartners_brochure.pdf')} target='_blank'>Brochure</BrochureLink></li>
          <li><StyledLink to='/Career'>Career at  G&P</StyledLink></li>
          <li><StyledLink to='/Contact'>Contact</StyledLink></li>
        </OuterUl>
      </div>
    )
  }
}

const StyledLink = g(Link)({
  textDecoration: 'none',
  color: 'white',
  ':hover': {
    fontWeight: 'bold'
  }
})
const BrochureLink = g.a({
  textDecoration: 'none',
  color: 'white',
  ':hover': {
    fontWeight: 'bold'
  }
})
const OuterUl = g.ul({
  listStyle: 'none',
  fontSize: '1.3rem',
  marginLeft: '3rem'
})
const SubMenuSwitchBlock = g.div({
  cursor: 'pointer',
  marginLeft: '-2rem'
})
const SubMenuSwitchIcon = g.div(
  {
    width: '1rem',
    height: '1rem',
    background: `url(${arrow})`,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    margin: '0 0.5rem',
    display: 'inline-block'
  },
  ({ isSubMenuOpen }) => ({
    transform: `rotate(${isSubMenuOpen ? '-90' : '90'}deg) translateX(6px)`
  })
)
const SubMenuSwitchText = g.span({
  color: 'white'
})
const InnerUl = g.ul(
  {
    listStyle: 'none',
    borderLeft: '3px solid rgba(255, 255, 255, 0.6)',
    margin: '1rem',
    paddingLeft: '1rem'
  },
  ({ isSubMenuOpen }) => ({
    display: `${isSubMenuOpen ? 'block' : 'none'}`
  })
)
const Close = g.img({
  width: '1.5em',
  margin: '1.25em',
  cursor: 'pointer'
})
