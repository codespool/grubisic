import React from 'react'
import g from 'glamorous'
import Link from 'gatsby-link'

import { uuid } from '../utils/shared'

export const HorizontalMenu = ({ serviceList }) => {
  return (
    <Wrapper>
      <MenuItem to='/SelectedProjects'>Selected projects</MenuItem>
      <MenuItem to='/AboutUs'>About us</MenuItem>
      {serviceList.map(service => (
        <MenuItem key={uuid()} to={`/services/${service.node.slug}`}>
          {service.node.title}
        </MenuItem>
      ))}
    </Wrapper>
  )
}

const Wrapper = g.div({
  position: 'fixed',
  top: '5rem',
  height: '3rem',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  width: '100%',
  background: '#282f39',
  zIndex: '3'
})
const MenuItem = g(Link)({
  textDecoration: 'none',
  color: 'rgba(255, 255, 255, 0.7)',
  cursor: 'pointer',
  margin: '0 1rem',
  fontFamily: 'Lato',
  ':hover': {
    color: 'white'
  }
})
