import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import g from 'glamorous'
import { navigateTo } from 'gatsby-link'
import { mediaQueries, uuid } from '../utils/shared'

export class TeamMembersGrid extends Component {
  state = {
    selected: null
  }
  componentDidMount () {
    for (let key in this.activePortraitRefs) {
      this.offsets[key] = this.getOffset(key)
    }
  }
  activePortraitRefs = {}
  offsets = {}
  handlePortraitClick = portraitId => {
    this.setState(currentState => ({
      selected: currentState.selected === portraitId ? null : portraitId
    }))
  }
  getOffset = index => {
    if (this.activePortraitRefs[index]) {
      const currentRef = ReactDOM.findDOMNode(
        this.activePortraitRefs[index]
      ).getBoundingClientRect()
      return 0 - currentRef.left
    }
    return 0
  }
  render () {
    const { team = { edges: [] } } = this.props
    return (
      <div>
        <Title><div>Our team</div></Title>
        <Wrapper>
          {team.edges.map((teamMember, index) => {
            return (
              <PortraitWrapper key={uuid()}>
                <Portrait
                  src={teamMember.node.image.sizes.src}
                  onClick={() => this.handlePortraitClick(index)}
                  ref={elem => {
                    this.activePortraitRefs[index] = elem
                  }}
                />
                <NameWrapper>
                  <div>{teamMember.node.name}</div>
                  <div>{teamMember.node.rank}</div>
                </NameWrapper>
                <CvWrapper
                  active={index === this.state.selected}
                  offset={this.offsets[index]}
                >
                  <div
                    dangerouslySetInnerHTML={{
                      __html: teamMember.node.education.childMarkdownRemark
                        .html
                    }}
                  />
                  <div
                    dangerouslySetInnerHTML={{
                      __html: teamMember.node.experience.childMarkdownRemark
                        .html
                    }}
                  />
                  <div
                    dangerouslySetInnerHTML={{
                      __html: teamMember.node.other ? teamMember.node.other.childMarkdownRemark
                        .html : ''
                    }}
                  />{' '}
                </CvWrapper>
              </PortraitWrapper>
            )
          })}
        </Wrapper>
      </div>
    )
  }
}

const Wrapper = g.div({
  display: 'grid',
  gridTemplateColumns: '1fr',
  gridAutoRows: 'auto',
  [mediaQueries.small]: {
    gridTemplateColumns: '1fr 1fr'
  },
  [mediaQueries.mid]: {
    gridTemplateColumns: 'repeat(4, 1fr)'
  },
  [mediaQueries.large]: {
    gridTemplateColumns: 'repeat(6, 1fr)'
  }
})
const CvWrapper = g.div(
  {
    position: 'relative',
    textAlign: 'left',
    width: '100%',
    zIndex: '20',
    left: '-200'
  },
  ({ active, offset }) => {
    return { display: active ? 'block' : 'none', left: offset }
  }
)
const CvDummy = g.div(
  {
    width: '1px'
  },
  ({ cvElemHeight }) => ({ height: cvElemHeight })
)
const PortraitWrapper = g.div({})
const NameWrapper = g.div({
  position: 'relative',
  top: '-100px',
  color: 'white',
  '& div:first-child': {
    textTransform: 'uppercase',
    fontFamily: 'Lato'
  }
})
const Portrait = g.img({
  width: '100%',
  filter: 'grayscale(1)',
  '&:hover': {
    filter: 'none'
  }
})
const Title = g.div({
  fontFamily: 'Lato',
  fontSize: '1.2rem',
  backgroundColor: 'rgb(150, 157, 169)',
  height: '3rem',
  width: '100%',
  color: '#282f39',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  '& div': {
    textTransform: 'uppercase',
    fontWeight: 'bold'
  }
})
