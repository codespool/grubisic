import React, { Component } from 'react'
import g, { Div } from 'glamorous'
import { MainHeader } from '../components/MainHeader'
import { MainMenu } from '../components/MainMenu'
import { TeamList } from '../components/TeamList'
import { MainFooter } from '../components/MainFooter'

export class Layout extends Component {
  state = {
    isMenuOpen: false,
    isTeamListOpen: false
  }
  toggleMenu = menuState => this.setState({ isMenuOpen: menuState })
  toggleTeamList = teamListState =>
    this.setState({ isTeamListOpen: teamListState })
  render () {
    const { children, serviceList, team } = this.props
    return (
      <Div minWidth='360px'>
        <MenuWrapper isOpen={this.state.isMenuOpen}>
          <MainMenu
            closeMenuHandler={() => this.toggleMenu(false)}
            serviceList={serviceList}
          />
        </MenuWrapper>
        {this.props.submenu}
        <TeamListWrapper isOpen={this.state.isTeamListOpen}>
          <TeamList team={team} closeMenuHandler={() => this.toggleTeamList(false)} />
        </TeamListWrapper>
        <ContentWrapper
          onClick={() => {
            if (this.state.isMenuOpen) this.toggleMenu(false)
            if (this.state.isTeamListOpen) this.toggleTeamList(false)
          }}
        >
          <MainHeader
            menuHandler={() => this.toggleMenu(true)}
            teamListHandler={() => this.toggleTeamList(true)}
            siteTitle={this.props.title}
          />
          <ChildrenWrapper>
            {children}
          </ChildrenWrapper>
        </ContentWrapper>
        <PageFooter>
          <MainFooter />
        </PageFooter>
      </Div>
    )
  }
}

const MenuWrapper = g.div(
  {
    position: 'fixed',
    width: '360px',
    transitionDuration: '.3s',
    height: '100%',
    color: 'white',
    zIndex: '10',
    background: 'rgba(241,16,30, 0.8)'
  },
  ({ isOpen = false }) => ({
    left: isOpen ? '0' : '-360'
  })
)
const ContentWrapper = g.div({
  width: '100vw',
  textAlign: 'center'
})
const PageFooter = g.div({
  width: '100vw',
  background: '#282f39'
})
const ChildrenWrapper = g.div({
  '& div>': {
    paddingTop: '5em'
  }
})
const TeamListWrapper = g.div(
  {
    position: 'fixed',
    width: '360px',
    transitionDuration: '.3s',
    height: '100%',
    color: 'white',
    zIndex: '10',
    background: 'rgba(241,16,30, 0.8)',
    overflow: 'scroll'
  },
  ({ isOpen = false }) => ({
    right: isOpen ? '0' : '-360px'
  })
)
