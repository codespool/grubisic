import React, { Component } from 'react'
import g, { Div } from 'glamorous'
import closeIcon from '../assets/icons/Close_hq.png'
import { func } from 'prop-types'
import { uuid } from '../utils/shared'
export class TeamList extends Component {
  static propTypes = {
    closeMenuHandler: func
  }

  render () {
    const { closeMenuHandler, team = { edges: [] } } = this.props
    return (
      <div>
        <Div textAlign='right'>
          <Close src={closeIcon} onClick={closeMenuHandler} />
          {team.edges.map(teamMember => (
            <NameCard key={uuid()}>
              <ProfileImage src={teamMember.node.image.sizes.src} />
              <InfoText>
                <Name>{teamMember.node.name}</Name>
                <Rank>{teamMember.node.rank}</Rank>
                <Contact>{teamMember.node.phone}</Contact>
                <Contact>{teamMember.node.mobile}</Contact>
                <Contact>{teamMember.node.email}</Contact>
              </InfoText>
            </NameCard>
          ))}
        </Div>

      </div>
    )
  }
}

const NameCard = g.div({
  display: 'flex',
  padding: '1rem 0.5rem',
  textAlign: 'left',
  alignItems: 'center',
  '& h3': {
    display: 'none'
  },
  ':hover': {
    backgroundColor: 'rgba(150,5,19, 0.7)',
    '& h3': {
      display: 'block'
    }
  }
})
const InfoText = g.div({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: '0 1rem'
})
const Name = g.h1({
  fontSize: '1.2rem',
  margin: '0'
})
const Rank = g.h2({
  fontSize: '1rem',
  margin: '0 0 1rem 0'
})
const Contact = g.h3({
  fontSize: '0.8rem',
  margin: '0 0 0.3rem 0'
})
const ProfileImage = g.div(
  {
    borderRadius: '50%',
    gridArea: 'image',
    alignSelf: 'center',
    width: '100px',
    height: '100px',
    margin: '0'
  },
  ({ src }) => ({
    background: `url(${src})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundPositionY: 0
  })
)
const Close = g.img({
  width: '1.5em',
  margin: '1.25em',
  cursor: 'pointer'
})
