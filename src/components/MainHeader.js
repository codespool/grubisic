import React from 'react'
import Link from 'gatsby-link'
import g from 'glamorous'
import logo from '../assets/icons/Grubisic_logo_hq.png'
import hamburger from '../assets/icons/Hamburger_hq.png'
import { string, func } from 'prop-types'
import { mediaQueries } from '../utils/shared'

export const MainHeader = ({ siteTitle, menuHandler, teamListHandler }) => (
  <Wrapper solidBack={!!siteTitle} >
    <Hamburger src={hamburger} onClick={menuHandler} />
    {siteTitle ? <TextTitle>{siteTitle}</TextTitle> : <LogoTitle to='/' />}
    <SideMenuWrapper>
      <TeamButton onClick={teamListHandler}>Team</TeamButton>
      <LanguageSwitch>HR</LanguageSwitch>
    </SideMenuWrapper>
  </Wrapper>
)

MainHeader.propTypes = {
  siteTitle: string,
  menuHandler: func,
  teamListHandler: func
}
const TeamButton = g.span({
  borderRight: '2px solid white',
  paddingRight: '0.5rem',
  cursor: 'pointer'
})
const LanguageSwitch = g.span({
  paddingLeft: '0.5rem',
  cursor: 'pointer'
})
const Wrapper = g.div({
  position: 'fixed',
  height: '5em',
  display: 'grid',
  gridTemplateColumns: '0.5fr 1fr 0.5fr',
  width: '100vw',
  background: 'rgba(125, 125, 125, 0.3)',
  alignItems: 'center',
  zIndex: 5,
  minWidth: '360px'
}, ({solidBack}) => ({
  background: solidBack ? 'rgb(241,16,30)' : 'rgba(125, 125, 125, 0.3)'
}))
const Hamburger = g.img({
  width: '1.2rem',
  maxWidth: '1.2rem',
  margin: '0 1rem',
  justifySelf: 'flex-start',
  cursor: 'pointer',
  [mediaQueries.mid]: {
    width: '1.5rem',
    maxWidth: '1.5rem'
  }
})
const SideMenuWrapper = g.div({
  margin: '0 2rem',
  justifySelf: 'flex-end',
  color: 'white',
  textTransform: 'uppercase',
  fontSize: '0.75rem',
  fontFamily: 'Lato',
  [mediaQueries.mid]: {
    fontSize: '1rem'
  }
})
const LogoTitle = g(Link)({
  background: `url(${logo})`,
  width: '100%',
  height: '70%',
  backgroundSize: 'contain',
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center',
  justifySelf: 'center'
})
const TextTitle = g.div({
  fontSize: '1.3rem',
  color: 'white',
  textTransform: 'capitalize',
  fontFamily: 'Source Serif Pro',
  [mediaQueries.mid]: {
    fontSize: '2rem'
  }
})
