import React from 'react'
import g from 'glamorous'
import { navigateTo } from 'gatsby-link'
import { mediaQueries } from '../utils/shared'
export const NewsThumb = ({ newsItem: { node: newsItem } }) => {
  return (
    <Wrapper
      onClick={() => navigateTo(`/news/${newsItem.slug}`)}
      css={{ background: `url(${newsItem.image.sizes.src})` }}
    >
      <Title>
        {newsItem.title}
      </Title>
    </Wrapper>
  )
}

const Wrapper = g.div({
  cursor: 'pointer',
  width: '100%',
  height: '100%',
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center',
  backgroundSize: 'cover !important',
  display: 'flex',
  alignItems: 'flex-end',
  color: 'white',
  [mediaQueries.mid]: {
    width: '90%',
    height: '90%'
  }
})
const Title = g.div({
  width: '100%',
  backgroundColor: 'rgba(0, 0, 0, 0.3)',
  padding: '1em',
  minHeight: '35%',
  textAlign: 'start',
  fontFamily: 'Lato',
  fontSize: '1.2rem'
})
