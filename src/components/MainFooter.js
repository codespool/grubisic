import React from 'react'
import g, { Div, Img } from 'glamorous'
import { mediaQueries } from '../utils/shared'
import DiceIcon from '../assets/icons/Dice.png'
import CompanyLogo from '../assets/icons/Grubisic_logo_hq.png'
import GlobalScopeIcon from '../assets/icons/GS_logo_white_transparent.png'
import LocationIcon from '../assets/icons/Location.png'
import PhoneIcon from '../assets/icons/Phone.png'
import MailIcon from '../assets/icons/Email.png'

export const MainFooter = () => {
  return (
    <Wrapper>
      <InfoSection>
        <Logo>
          <img src={DiceIcon} />
          <img src={CompanyLogo} />
        </Logo>
        <GlobalScope>
          <Div display='flex' flexDirection='column' justifyContent='center' maxWidth='90%'>
            <span>grubisic & partners is member of</span>
            <img src={GlobalScopeIcon} />
          </Div>
        </GlobalScope>
        <Address>
          <Div display='flex' >
            <Img margin='1rem' marginLeft='0' src={LocationIcon} />
            <Div display='flex' flexDirection='column'>
              <span>Zadarska 80</span>
              <span>10000 Zagreb, Croatia</span>
            </Div>
          </Div>
        </Address>
        <Contact>
          <Div display='flex'>
            <img src={PhoneIcon} />
            <h4>+385 1 789 7120</h4>
          </Div>
          <Div display='flex'>
            <img src={MailIcon} />
            <h4>info@grubisic-partneri.net</h4>
          </Div>
        </Contact>
      </InfoSection>
      <Copyright>
        <span>GRUBISIC & partners © 2018</span>
        <span>Privacy policy</span>
      </Copyright>
    </Wrapper>
  )
}

const Wrapper = g.div({
  display: 'flex',
  flexDirection: 'column',
  width: '100vw'
})
const InfoSection = g.div({
  width: '100vw',
  display: 'grid',
  paddingTop: '1rem',
  gridTemplateRows: 'repeat(4, 1fr)',
  gridGap: '10px',
  background: '#282f39',
  alignItems: 'flex-start',
  [mediaQueries.mid]: {
    gridTemplateRows: 'repeat(2, 1fr)',
    gridTemplateColumns: 'repeat(2, 1fr)'
  },
  [mediaQueries.large]: {
    gridTemplateColumns: 'repeat(4, 1fr)',
    gridTemplateRows: '1fr'
  }
})
const Copyright = g.div({
  background: 'rgb(241,16,30)',
  color: 'white',
  display: 'flex',
  justifyContent: 'space-between',
  padding: '1rem',
  fontFamily: 'Lato',
  fontSize: '0.9rem'
})
const Logo = g.div({
  width: '100vw',
  maxWidth: '450px',
  display: 'flex',
  alignItems: 'flex-start',
  justifyContent: 'space-between',
  '& img': {
    width: '50%',
    margin: '0 1rem',
    '&:first-child': {
      width: '30%'
    }
  }
})
const GlobalScope = g.div({
  maxWidth: '80%',
  color: 'white',
  textTransform: 'uppercase',
  fontSize: '0.8rem',
  display: 'flex',
  justifyContent: 'center',
  margin: '0 1rem'
})
const Address = g.div({
  display: 'flex',
  fontFamily: 'Lato',
  color: 'white',
  margin: '0 1rem'
})
const Contact = g.div({
  fontFamily: 'Lato',
  color: 'white',
  margin: '0 1rem',
  '& h4': {
    marginLeft: '1rem'
  }
})
