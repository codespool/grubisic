import React from 'react'
import g from 'glamorous'
import { navigateTo } from 'gatsby-link'
export const NewsCard = ({ newsItem: { node: newsItem } }) => {
  return (
    <Wrapper
      onClick={() => navigateTo(`/news/${newsItem.slug}`)}
    >
      <HeroImage css={{ background: `url(${newsItem.image.sizes.src}) no-repeat center center` }} />
      <TitleBlock>
        <Title>{newsItem.title}</Title>
        <NewsDate>{newsItem.displayDate}</NewsDate>
      </TitleBlock>
      <NewsText>
        {newsItem.content.childMarkdownRemark.excerpt}
      </NewsText>
    </Wrapper>
  )
}

const HeroImage = g.div({
  height: '50%',
  minHeight: '50%',
  width: '100%'
})
const Wrapper = g.div({
  border: '1px solid #949daa',
  display: 'flex',
  flexDirection: 'column',
  cursor: 'pointer',
  width: '300px',
  height: '400px',
  overflow: 'hidden',
  margin: '1rem'
})
const TitleBlock = g.div({
  textAlign: 'left',
  padding: '10px'
})
const Title = g.h1({
  fontSize: '1.2rem',
  textTransform: 'uppercase',
  margin: '0'
})
const NewsDate = g.h2({
  fontSize: '0.8rem',
  margin: '0',
  color: 'rgb(241,16,30)'
})
const NewsText = g.p({
  textAlign: 'left',
  height: '150px',
  fontSize: '1rem',
  padding: '10px'
})
