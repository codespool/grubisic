import React from 'react'
import Helmet from 'react-helmet'
import { Div } from 'glamorous'

import './index.css'
export default function Layout (props) {
  const {data: {services: {edges: serviceList}, team}} = props
  return (
    <Div minWidth='360px'>
      <Helmet
        title={props.data.meta.siteMetadata.title}
        meta={[
          { name: 'description', content: 'Sample' },
          { name: 'keywords', content: 'sample, something' }
        ]}
      />
      {props.children({...props, serviceList, team})}
    </Div>
  )
}

export const query = graphql`
  query layoutDataQuery {
    meta: site {
      siteMetadata {
        title
      }
    },
    services: allContentfulServices{
    edges {
      node {
        title,
        slug
      }
    }
  },
  team: allContentfulTeamMembers(sort: {fields: [createdAt]}){
    edges {
      node {
        name
        rank
        phone
        mobile
        email
        education{
          childMarkdownRemark{
            html
          }
        }
        experience{
          childMarkdownRemark{
            html
          }
        }
        other{
          childMarkdownRemark{
            html
          }
        }
        image{
          sizes(maxWidth: 500, maxHeight: 500){
            src
          }
        }
      }
    }
  }
  }
`
