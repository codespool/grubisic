module.exports = {
  siteMetadata: {
    title: 'Grubisic & partneri test'
  },
  plugins: [
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: `nu1l8h3e3rc9`,
        accessToken: `294914e5e67782c745a33f858978e582c04e92b77d908fb2e991c2fbdb63e143`
      }
    },
    'gatsby-plugin-react-helmet',
    `gatsby-plugin-glamor`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography.js`
      }
    },
    `gatsby-transformer-remark`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`
  ]
}
